---
uuid:             365e5478-a3ba-4f65-80b2-3cd22e071b2c
layout:           post
title:            纪录片：被误解的女性
slug:             movie-miss-representation
subtitle:         null
date:             '2015-03-05T04:59:07.000Z'
updated:          '2015-07-31T06:58:25.000Z'
author:           'Yvan Hom'
author_slug:      yvan
header_img:       null
status:           published
language:         en_US
meta_title:       null
meta_description: null
tags:
  - 3d-world
  - movies

---


纪录片，讲述美国的女性现状，附上[豆瓣链接](http://movie.douban.com/subject/5973966/)。

一直想了解女权是什么，怎样才算是男女平等，可是一直没静得下心来阅读。

无意中看了这部纪录片，冲击深刻，对自己以前的关于男女平等的总总认识，有了新的审视。



