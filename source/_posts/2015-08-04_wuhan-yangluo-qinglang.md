---
uuid:             51c5edd5-0e8e-40fe-bee6-ae1cdb8bf0fb
layout:           post
title:            武汉，蓝天白云，晴朗的一天
slug:             wuhan-yangluo-qinglang
subtitle:         null
date:             '2015-08-04T21:25:22.000Z'
updated:          '2016-07-04T08:04:35.000Z'
author:           'Yvan Hom'
author_slug:      yvan
header_img:       null
status:           published
language:         en_US
meta_title:       null
meta_description: null
tags:
  - 3d-world

---

在武汉，天气晴朗时，蓝天白云，望着天空，感觉心情畅快，舒服无比。

这个比先前时而下雨，时而昏暗的天空，舒服多了。

想起岳阳楼记。

> 若夫淫雨霏霏，连月不开，阴风怒号，浊浪排空；日星隐曜，山岳潜形；商旅不行，樯倾楫摧；薄暮冥冥，虎啸猿啼。登斯楼也，则有去国怀乡，忧谗畏讥，满目萧然，感极而悲者矣。

> 至若春和景明，波澜不惊，上下天光，一碧万顷；沙鸥翔集，锦鳞游泳；岸芷汀兰，郁郁青青。而或长烟一空，皓月千里，浮光跃金，静影沉璧，渔歌互答，此乐何极！登斯楼也，则有心旷神怡，宠辱偕忘，把酒临风，其喜洋洋者矣。

要做到“不以物喜，不以己悲”太难了，还是凡夫俗子，“先天下之忧而忧，后天下之乐而乐”，是追求的境界，目前还是先为武汉的好天气高兴高兴。

虽然很热，不过流点汗水也是很畅快的。

来两张路过阳逻时的图片。

![武汉阳逻的天空1](http://o9sak9o6o.bkt.clouddn.com/sites/wuhan-1.jpg)

![武汉阳逻的天空2](http://o9sak9o6o.bkt.clouddn.com/sites/wuhan-2.jpg)
