---
uuid:             f969b06c-21ca-4bd2-b64f-31a4f786221d
layout:           post
title:            读书：春节假期书单
slug:             book-booklist
subtitle:         null
date:             '2015-01-26T15:14:48.000Z'
updated:          '2015-07-31T06:55:28.000Z'
author:           'Yvan Hom'
author_slug:      yvan
header_img:       null
status:           published
language:         en_US
meta_title:       null
meta_description: null
tags:
  - 3d-world
  - reading

---


2015年的春节可以多放几天，从图书馆借了几本书，计划假期读完，有：

- [孤寂深渊](http://book.douban.com/subject/1148508/)
- [莫班小姐](http://book.douban.com/subject/2364639/)
- [她是个弱女子](http://book.douban.com/subject/3036888/)
- [达洛卫夫人](http://book.douban.com/subject/2174400/)
- [秀拉](http://book.douban.com/subject/25820440/)
- [怜香伴](http://book.douban.com/subject/10432702/)

此外，还有图书馆没有的，在网上找了电子书：

- [请在天堂等我](http://book.douban.com/subject/4848836/)
- [比利提斯之歌](http://book.douban.com/subject/3672259/)
- [阿芙洛狄特](http://book.douban.com/subject/4857094/)

貌似有点多啊，尽力而为。

估计最后只看了一本，也说不定。



