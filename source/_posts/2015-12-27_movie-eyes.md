---
uuid:             53068eeb-9980-4a59-9212-966a4750b07c
layout:           post
title:            电影：诡眼
slug:             movie-eyes
subtitle:         null
date:             '2015-12-27T22:55:56.000Z'
updated:          '2015-12-27T22:55:56.000Z'
author:           'Yvan Hom'
author_slug:      yvan
header_img:       null
status:           published
language:         en_US
meta_title:       null
meta_description: null
tags:
  - 3d-world
  - movies

---

由于“午夜凶铃”而怀着要好好被吓一次的心情看完了这部电影，没有被吓到，失望，这不是鬼片，更不是恐怖片，就是一悬疑片，还不是惊悚片。

想要表达什么呢？命运吧。

生命如此脆弱，意外，事故，生命的终结那么突然，不可重来，无法回退，过去只能过去，结束只能接受，无疑只能无力地用命运来归纳。

这都是命运啊，还能有什么办法，只能接受。不接受，也没有办法。改变命运什么的，压根不可能，没有超能力，没有哆啦 A 梦，没有时光机器，没有备份，没有神龙，我们都是普通人。

生死的命运，毫无办法。

“全部称为 F”中，四季说过，没有人是自愿降临这个世界的。

我们的出生，朦胧无知，我们的离去，充满命运的悲剧。

最喜欢的还是开场好友死去那段。等待主角的好友，放过了一班公交，然后在下一班公交到来时经主角劝说而上了这一班死亡公交。公交上，好友不经意掉落的物品滚到司机附近，在捡起的时候，发现司机已经死亡，电车在盲目地向前，好友那一瞬间的无奈，无助，恐慌，开玩笑吧……

什么多没做，也依然中了命运的枪。上与不上，随意的一个选择，便是生与死的抉择。如此的戏剧性，也只能是命运。

如果这时候有传教士跟她讲上帝，那么她会相信的吧。


