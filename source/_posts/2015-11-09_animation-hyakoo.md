---
uuid:             4c32a929-667f-4f0e-8fe1-f2a35fc566f6
layout:           post
title:            动漫：虎子
slug:             animation-hyakoo
subtitle:         null
date:             '2015-11-09T20:45:51.000Z'
updated:          '2015-11-09T20:45:51.000Z'
author:           'Yvan Hom'
author_slug:      yvan
header_img:       null
status:           published
language:         en_US
meta_title:       null
meta_description: null
tags:
  - 2d-world

---

看完第一集，着实被惊艳了一把，赞！这脑洞，路痴四人组，笨蛋四人组，迷路的乖乖女，遇上迷路的脾气不好的从小学呆到高中的大小姐，持续迷路的状态下，再遇上元气笨蛋虎子，和三无吃货小雀，主角队集齐了，一起直线前进。

一切都是那么美好。

可是接下来的几集完全没有第一集的感觉，节奏不对？笑点太过生硬？不过还是有一口气追到底的激情。

直到最后一集，又精彩了，回到最初她们即将相遇的时候。

我是因为看视频剪辑中最后一集的 Kiss 才追的，果然，虎子和小雀是真爱。

小雀三无，吃货，怪力，小天使，很戳我萌点。

不过我最喜欢的还是大小姐龙姬，虽然脾气不好（虎子这么作死，换谁都脾气不好吧），虽然嘴上拒人，但是就是萌啊，可惜作为四人组之一，戏分不足啊。
