---
uuid:             c5025410-7af4-4104-a853-778df7cf0b58
layout:           post
title:            电影：珍妮的婚礼
slug:             movie-jennys-wedding
subtitle:         null
date:             '2015-11-05T02:28:28.000Z'
updated:          '2015-11-05T02:28:28.000Z'
author:           'Yvan Hom'
author_slug:      yvan
header_img:       null
status:           published
language:         en_US
meta_title:       null
meta_description: null
tags:
  - 3d-world
  - movies

---

好奇怪，记得这个是已经写好，并发布了，可是还是草稿，并且是空的，只能凭借着记忆写一遍。

温馨的电影。

出柜需要勇气，家人的态度是最不稳定的因素，家人理解那么皆大欢喜，否则遍地纠结。

不顾他人看法地活着，很艰难。

家人的压迫，生活在谎言中，用一个谎言掩盖另一个谎言，无穷无尽。

回头再看，其实真实地活着才是美好。

最终珍妮作出了抉择，走自己的路，并不断得到家人的理解，祝福，因为她是真心活得开心快乐，自由自在。

[豆瓣链接](http://movie.douban.com/subject/25761564/)
