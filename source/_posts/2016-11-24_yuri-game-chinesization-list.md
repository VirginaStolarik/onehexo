---
uuid:             60e6bde4-bd03-40e3-a7ed-1da0823d42e1
layout:           post
title:            百合英文游戏汉化
slug:             yuri-game-chinesization-list
subtitle:         null
date:             '2016-11-24T22:43:27.000Z'
updated:          '2016-11-25T03:54:42.000Z'
author:           'Yvan Hom'
author_slug:      yvan
header_img:       null
status:           published
language:         en_US
meta_title:       null
meta_description: null
tags:
  - 2d-world
  - yuri-game-chinesization

---

### Her Tears Were My Light

**中文名**: 她的眼泪曾是我的光芒

**描述**

“她的眼泪曾是我的光芒”是一部视觉小说，参与 NaNoRenO 2016，讲述着一个 Time 和 Space 间的简短爱情故事，您可以向前跳跃时间，回退时间，重回任意存档的时间点，那么您是否能用这股力量拯救 Space 的孤独？

**下载**

百度云 [1bpED0JH](https://pan.baidu.com/s/1bpED0JH)，提取密码 2vq7。

### The Sad Story of Emmeline Burns

**中文名**: 幽灵悲情 / 艾米琳·伯恩斯的悲惨往事

**描述**

托玛·安德鲁斯，14岁，一直像个外来者，喜欢花时间呆在墓地里，感觉呆在死者中间比呆在活人中间更舒适。

直到有一天，托玛在当地的墓地里，邂逅了一个古怪的女孩，她可能是真正的幽灵。

那女孩邀请托玛逗留片刻，听她讲个故事，关于一个叫艾米琳·伯恩斯的女孩的故事，她在1851年的时候悲惨地死去。

托玛不由自主地沉迷，但同时她感觉到不安。毕竟，她能做什么呢？她怎样能够帮助到一个已经死去的人？

**下载**

百度云 [1nvuarCd](https://pan.baidu.com/s/1nvuarCd)，提取密码 xxea。

### Romance Detective

**中文名**: 浪漫侦探

**描述**

浪漫侦探是视觉小说游戏，最初是制作参加 NaNoRenO 2014，讲述解决激情犯罪的侦探跟她的搭档浪漫警官的关于爱的故事。

**下载**

百度云 [1kVx2gPD](https://pan.baidu.com/s/1kVx2gPD)，提取密码 a3kc。

### Princesses's Maid

**中文名**: 公主女仆

**描述**

孪生公主姐妹们即将在13天后为了她们的王国嫁给她们完全不认识的人。她们才小到大的朋友，区区一个无能为力的女仆，能做什么呢？

**下载**

百度云 [1dF3ea0x](https://pan.baidu.com/s/1dF3ea0x)，提取密码 ijjw。

### Written in the Sky

**中文名**: 书之天宇

**描述**

作为侦探的女儿，艾瑞莉一直是个好奇心旺盛的女孩。

当她见到不同寻常的事物，她就有股冲动，想要密切关注，弄清她所见到的究竟是什么。

但在这个特殊的夜晚，艾瑞莉的好奇心却为她埋下了祸根。

她跟踪着一可疑的身影，偶然见到一刻着陌生文字的奇异的黑盒子，盒子里，她找到一枚小戒指，上面同样刻着陌生的文字。艾瑞莉不加思索就把戒指戴上，但这时，她身后传来脚步声。

受到惊吓的艾瑞莉夺步而逃。她跑回家中，检查着戒指，困惑她为何要为这件小饰物而担上危险。那是FBI从星际和平会议偷来的外星婚戒，它是给那些星球带来和平的关键。作为火星帝王的女儿，西恩娜来到地球进行寻找。

但戒指已经选好了她的主人，艾瑞莉，并跟她永远地绑定在一起。因为这，西恩娜也跟这命中注定是她的爱人的人类少女产生了羁绊。

**下载**

百度云 [1c24lSa](https://pan.baidu.com/s/1c24lSa)，提取密码 gpep。


