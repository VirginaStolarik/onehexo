---
uuid:             8662a5d1-6004-46d4-8ee7-4eb96a6f2370
layout:           post
title:            电影：新天龙八部之天山童姥
slug:             movie-dragon-chronicles-the-maidens-of-heavenly-mountain
subtitle:         null
date:             '2015-09-14T06:43:38.000Z'
updated:          '2015-09-14T06:43:38.000Z'
author:           'Yvan Hom'
author_slug:      yvan
header_img:       null
status:           published
language:         en_US
meta_title:       null
meta_description: null
tags:
  - 3d-world
  - movies

---

女神啊，女神，都是女神啊！

[豆瓣链接](http://movie.douban.com/subject/1297517/)

> 曾经沧海难为水，除却巫山不是云

巫行云，李秋水，李沧海，三位女神轻歌曼舞，饮酒作乐，实在美哉。

可惜来了个逍遥子。电影没放出逍遥子的模样，毫无疑问，没有什么男人能够跟三位女神站在同一画面而不被比下去，所以只能神隐。

可惜来了个逍遥子。

> 曾经沧海难为水，除却巫山不是云

可是巫行云和李沧海不能像诗句中一样相伴，李沧海喜欢的是逍遥子，于是巫行云的恋爱变成了苦恋，恋而不得，直至李沧海死去，李沧海想的也是逍遥子，没有留下什么对巫行云说。

最喜欢的是巫行云。尤其是小孩子的模样，啊，萝莉控暴露了。

> 人生如雾亦如梦，缘生缘灭还自在

回首过往，似梦似幻，斗了一辈子的人到最后却是联手的盟友，曾经喜欢的人已经离去，这世间还有什么可以留恋的。
