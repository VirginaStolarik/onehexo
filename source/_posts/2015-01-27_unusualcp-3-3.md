---
uuid:             e3532170-57c8-4859-9baf-364a16b35c1b
layout:           post
title:            '不一样的黑琴 3 相知（下）'
slug:             unusualcp-3-3
subtitle:         null
date:             '2015-01-27T20:08:11.000Z'
updated:          '2015-07-31T06:55:40.000Z'
author:           'Yvan Hom'
author_slug:      yvan
header_img:       null
status:           published
language:         en_US
meta_title:       null
meta_description: null
tags:
  - 2d-world
  - fanfiction
  - unusual-cp

---


(1)

离开图书馆时，御坂美琴是沮丧非常的。如果那本书不是作者为了圈钱的骗人产物，那么她实实在在地喜欢着黑子，是恋人式的喜欢。

虽然认识黑子才只有短短的几周时间，但是，御坂已经完全习惯并留恋有黑子陪伴的生活，虽然嘴上说离开常盘台也没关系，可是心底里却很害怕不能再跟黑子同住一个宿舍。

跟黑子的点点滴滴，御坂都能清晰地回想起来。

初次相见，黑子在她危险的时候出现，即使在被她坑了的情况下，依然可靠地保护了她，从那时候起，她便期待着跟黑子的重逢，希冀着黑子带来的安全感。

第二次见面，黑子在她为虚假的病痛折磨时出现，带来她健康的实情。

御坂想起了这几周的快乐时光，她跟黑子结识，同住，特训，玩耍，御坂更加熟悉黑子，了解黑子。

黑子是个热心人，会帮助萍水相逢的学姐，黑子热情似火，总是跟她那么亲近，黑子细心体贴，顾虑着她的感受，黑子坦率可爱，就连吐槽的声音都那么有磁性。

不知从什么时候开始，黑子填充了她的所有，或许从第一次见面开始，御坂便将黑子放在心上，放在最重要的位置。

喜欢用手抚摸着娇小的黑子那柔顺的头发，沉醉于黑子亲切触电般的拥抱，喜欢黑子在狂热举动被拒绝后不甘心的表情。

黑子的一切都是那么的可爱，那么地让御坂美琴喜欢。

可是黑子呢？黑子又是怎么看待御坂美琴的？

喜欢，所以她们成为了好朋友，可是，却只有朋友式的喜欢。

黑子的热心是本性，黑子的热情是对电击成瘾，黑子的体贴是对前辈的敬爱。

“黑子跟前辈只是朋友。”黑子对初春跟佐天的玩笑总是这么说，说的是那么的认真。

不想只做朋友的只有她一个。

单恋，完全没有希望的单箭头。

黑子恋爱的是常盘台的女王大人，那是御坂美琴所无法挑战的超能力者。

而现在，在另一处，黑子跟女王大人正在亲密地约会。

她该怎么办？她的初恋在她刚有所察觉时便已经注定失败，是放在心底默默地观望等待，是当作没有萌发过一样地扼杀掉，还是飞蛾扑火地跟女王竞争？

不知道，她现在只想见到黑子的容颜，听听黑子的声音，牵起黑子的手，感受黑子的陪伴。

想望那么强烈，御坂美琴已经无法再压抑着呆在图书馆，虽然不知道黑子去了哪里，可是她必须出发去寻找。

她想起了那个梦，黑子消失了，她疯狂地寻找。现在，她放任着自己没有路标地前进，到哪里都无所谓，只要有黑子在便可以了，可是黑子又在哪里呢？

见到黑子，该说什么？完全不清楚，可是如果只找到黑子一个人的话那该多好，黑子心系女王将只是她的瞎想，到时，她能够把内心想法吐露出来吗？如果黑子跟女王在一起，那么她能正常地打声招呼吗？

不知道，她只想找到黑子，让黑子出现在她的视线内。

(2)

白井黑子看着眼前所剩不多的美食，口水都快要流下来了，眼睛不舍地从食物上移开，瞄了一眼对面的食蜂操祈，发现食蜂一脸已经玩够了的表情，松了一口气，不顾形象地大快朵颐。

黑子直觉，现在在食蜂面前，不用太过拘束。

食蜂操祈也确实不介意，那是自然的黑子，这让她对无敌黑子有了更多的了解。

跟人相处，如果不知道对方在想什么，食蜂操祈总会觉得担忧恐惧，于是依靠心理掌控的能力读取对方的心思，只有确认了对方的意图，她才能够安心。

第一次见面，她便注意到了这个新生，于是一如既往地读心，结果没有读到任何有用的信息。

原来她便是无敌黑子，原来无敌黑子这么娇小可爱，是一个让她忍不住想去欺负的学妹。

可是，她不知道黑子怎么想她的。

不知道对方对她的看法，会让她很不安心。

可是，黑子却是不同的，黑子给她的感觉便是十足的安全可靠，即使她无法读取黑子的心思，即使黑子总是在脑海中欺负她。

她已经对各种各样的胡思乱想习以为常，所以黑子的脑欺完全是小儿科，但是看着黑子对自己的脑欺负觉得难为情，她觉得黑子很有意思，欺负人，肯定不是黑子会做的。

于是，她便期盼着能够更多地了解黑子，无论是无敌黑子，还是白井黑子，期盼着跟黑子的再次相见。

可是，不断地见面，却还是如同陌生人，她没有踏出搭话的第一步，不知道该说什么。

直到今天，以她战胜无敌黑子为契机，让无敌黑子卑躬屈膝地陪她一下午。虽然不知道获胜的战斗是什么，但是她确实赢了。

看着黑子明明不情愿，却还是努力装出一副听话的模样，再加上那股天生的安心感，都让食蜂操祈觉得，无论怎么欺负无敌黑子都是没问题的。

今天她玩得很高兴，所以，现在，无论黑子拜托她什么，她都会尽全力而为，绝不推脱。

不一会，餐桌上的食物便给黑子风卷残云地收走。食蜂操祈喊来服务员，再加了点，反正都是黑子付钱，她都已经请看电影了。

“菜还需要再等会，可以好好讲讲了吧。”

“是关于御坂美琴前辈的事。御坂前辈由于能力开发的事，就要被学校退学了。”

“噢，我知道。先前我也找学校上层谈过，不过只是给了一个月的时间，就不再退步了。”

“多谢了，食蜂。”

不知不觉，黑子便叫得那么亲近，不过食蜂操祈并没有觉得不适，或是奇怪。

“不过，常盘台里居然也有不听常盘台女王话。”

“是啊，这都怪有学院都市的无敌黑子袒护，让本女王不能放开手脚施以惩戒！”

黑子无语。看着吃瘪的黑子，食蜂觉得黑子果然有意思。

“不过，我也没放着不管，到时候如果学校胡来，便把他们全洗脑就是了，不过这个，御坂同学可没有赞同。怎么，改变主意了？是要拜托我这个？”

“才不是呢，才不会像你那么胡来，要胡来的话，我自己也可以做到。”

也是啊，无敌黑子胡来，谁能管住？

“那么怎么办？”

“能用道理讲通的事情，我从不用能力胡来。”

不胡来，这难道是黑子给她安全感的原因？

“跟那些老油条讲道理？他们都是从学校讲台一步步爬上去的，一辈子给人讲大道理，要把道理讲给他们听，这得多难，还是洗脑容易点。”

“要是容易，也不会来拜托女王大人了。”

“这个恭维可无效。”

“怎么是恭维，女王大人的话，常盘台没人不好好听听的。”

“无敌黑子的话，整个学院都市都得听听。”

“跟无敌黑子惹上关系，可不是好事，会给御坂前辈带来不少麻烦。可是女王大人就不一样了，大家都知道女王大人喜欢管闲事。不过，这对黑子来说，可不是闲事，是非常重要的事。”

丫的居然不否认，虽然食蜂也不能否认无敌黑子对学院都市的影响力？既然这样，她也就管管黑子重要的事吧。

“道理，有什么道理好说的呢？”

“学校并没有拒绝低能力者就读的约定，就算是入学也没有。当年闹大的拒绝某王女入学，也并不是因为能力低者不能入学，而是因为她排名太靠后了。是的，常盘台成为名校后，拥有优秀的生源，每年都能招收到足够的能力高者。如果当时那个政治家低调点，再加个名额又有何妨，可是太过高调，惹得谁都知道，学校也就只能强硬，表示教育基地的不屈。”

“还是面子问题吧。”

“嗯，现在的情况也类似，都市传说，让学校感到尊严受到挑战，没面子。”

“真是吃饱了没事做，都怪你把他们都保护得太好了。”

“哪有，我只对付不法分子。回到正题，学校的校训，建校宗旨等，都强调公平，人文精神，育人为本，怎么说，学校的这个决定都是错误的，开了这个先例，只怕学校真把拒绝低能力者当规定了。”

“说道理，真麻烦啊……”

“没关系，失败了也没事，到时无敌黑子会由于好朋友食蜂操祈的受挫而出马。”

她跟无敌黑子扯上关系就没问题是吧？就像恒温死神。不过说得好像她会失败一样，真是无礼啊，黑子。

“哼，等我好消息吧。”

“真是感谢。”

看着黑子笑开了花，食蜂操祈感觉自己中了激将法，激将就激将吧，等会还要好好复习一下校训，办校宗旨，校规校史，一定要把那些老头说得头晕脑晕不可。一想到那些老头被她忽悠，她便觉得很高兴。

“菜来了，我又开动了，你还要再吃不？”

(3)

御坂美琴也不算漫无目的地寻找着黑子。

商场，没有看到黑子逛街的身影。—-此时黑子快把电影看完。

电影院，没有发现黑子的进出。—-此时黑子已经转战料理屋。

咖啡厅，奶茶馆，甜品店，没有黑子约会的场景。

虽然她希望黑子此时是一个人，在办黑子说的重要的事，可是却总是去电影、书本中出现的经典约会场地寻找。

没有发现黑子，她觉得很高兴，仿佛黑子并没有跟女王大人在约会，可是她确实想要跟黑子见面，明明才分开不到几个小时。

矛盾，既不想在这些场景见到黑子，见到的话表明黑子跟女王确实在约会，可是又想见到黑子，以慰单思之苦，可是却又只在王道约会地点寻找。

究竟是想见还是不想见？她究竟想要的是什么？

想要的是黑子的喜欢，最最喜欢，不止是她在寻找，她希望的是黑子能感受到她的喜欢，也来找她，找到她。

那时，便是相恋的两人找到对方，然后便像童话故事里说的，两人过上了幸福的生活。

不知觉间，天色已晚，御坂美琴想到此时黑子可能已经回宿舍了，黑子离开时，说的是“晚上回来再细说”。

黑子会跟她说什么呢？ 说跟女王大人的甜蜜约会？

往学校方向走，远远地看到校门口附近有人，她看到了黑子可爱的身影，而黑子并没有发现她，黑子正跟女王大人忘情地相抱在一起。

画面太美好，御坂美琴默默地走开，此时，她才发现她还没有吃晚餐，她的肚子已经饿扁，她要大吃一顿，暴饮暴食。

没有返回学校，进学校可能会被亲密中的两人发现，她去校外觅食，虽然时间不早了，晚归会受到宿监的的惩罚，可是已经没关系了。

已经没有什么有关系了。

(4)

吃完饭，直到回到校门口，食蜂操祁才大发慈悲地允许白井黑子直接将各种物件传输到她的宿舍，从各种负累中解脱的黑子忍不住为之欢呼。

“解放了！”

“真是的，一点小事便喊苦喊累，真是没有诚意。”

搬东西的不是你，站着说话不腰疼，黑子忍不住在心底里吐槽，当然嘴上是不会说出来的。

“那就到这里吧，下次一定好好表示我的诚意。”黑子觉得不会有下次了。

“嗯，拜。”

貌似女王还很不舍得，不过黑子可不会不舍得，黑子转身离去，要去找姐姐大人，几个小时不见，想念了。

不过刚迈出几步，黑子便被人从背后追上，拉住了她，黑子回过身，只见常盘台的女王大人少有的一副欲言要止的犹豫模样，黑子大为疑惑，食蜂操祁怎么这么反常？

虽然想问清楚，却又觉得还是等待比较好。

莫名的尴尬，最后还是被打破，犹豫再三的食蜂操祁终于开了口。

“为什么是她？”

食蜂问的问题是什么意思，黑子一时没有明白。

“为什么是御坂美琴？”

黑子这下多少有点明白了。她的心上人是御坂美琴，是姐姐大人，而不是跟她并肩作战，为共同理想而一起努力的恒温死神初春饰利，不是喜欢找她茬的婚后光子，不是她悉心教导的徒弟结标淡希，不是一直神交已久的常盘台女王食蜂操祁。

为什么呢？黑子也不清楚，也没去想过，喜欢便是喜欢，刚开始只是留恋姐姐大人的能力，随着深入地相处，她已经适应并不再那么沉溺那种能力，可是心的悸动却越来越强烈，已经不知不觉对姐姐大人产生了爱恋之意。

黑子喜欢御坂美琴，已经再也无法再喜欢别人了。

无法用喜欢回应喜欢，这是对喜欢黑子的人不可避免的伤害。

黑子不想伤害任何人，尤其是热心肠，狡黠有趣的食蜂女王。

食蜂操祁喜欢她吗？不喜欢，为什么要如此反常地问这个问题？

她能回答什么？只能回答心里话。食蜂女王是值得爱的朋友，黑子无法爱她，却珍惜这份友谊。

黑子靠近食蜂，用手环绕食蜂落在食蜂的背上抱住女王，将头靠在食蜂操祁硕大的胸上。

软绵绵，很有弹性，真是舒服。

能够平天下的白井黑子也希望自己将来也能聚人心。

“黑子，你…”

“听听读读我的答案，食蜂。”

靠在食蜂胸上，听着食蜂紊乱的心跳，黑子内心却一片平静，因为对方不是御坂美琴，如果是御坂美琴，那么即使只是牵手，即使只是脑海中想想，黑子都会产生无法抑制的情感冲动，想要跟心爱的人更亲近，让彼此的心靠拢在一起。

跟姐姐大人的点点滴滴一直记在黑子的心上，愉悦，傲娇，任性，难过，失落，一经想起，无不牵动着黑子的心。

黑子喜欢御坂美琴，没有什么理由，仅仅因为是御坂美琴，那个让黑子魂牵梦萦无法忘怀希望能够永远陪伴左右的女孩子。

黑子感觉自己的心也乱了，不是因为抱着女王，而是想起了姐姐大人。

黑子放开所有的防御，让食蜂操祁可以知道她心灵深处的想法，可以感受她身体上的回应。

渐渐地，黑子感觉到食蜂女王的心跳渐趋平稳，无法得到回应的心，也只能回归常态，黑子希望这颗心不久也能再次悸动，为别的值得喜爱的人。

“我明白了。”

“希望你也能找到心爱的人，食蜂。”黑子松开抱着的手，有点不舍地放弃胸枕，当然只是有点，黑子更喜欢的是抱着姐姐大人。

“当然。不过，黑子，你叫错了。”

“是，黑子知错，女王大人。”这次是真诚的，黑子不得不认可，食蜂操祁确实是常盘台的女王大人，不久也会是学院都市的女王大人，乃至是整个世界的女王大人。

“没想到无敌黑子这么好欺负，就这样被骗了。”

黑子只想收回刚刚的想法，食蜂操祁还是没有长大的顽童。

“能对无敌黑子成功读心，真是梦寐以求啊。”

“就知道嘴硬，看以后谁会喜欢你！”

“这个不用你担心，到时候，会让你叫上一声…嗯…对了，亲王大人的。”

“走着瞧，到时候谁大谁小还未知。”黑子是不用担心的，因为食蜂操祁的心智怎么看都长不大。

多年之后，当食蜂操祁带着女朋友出现在黑子面前时，那场景是现在的她们所无法想象的。

(5)

御坂美琴不知道要去哪好，只想离沉溺在幸福中的两人远点，漫无目的地飘，不知不觉进了一家料理店，本想一个人点满三个人的份，痛痛快快地吃一顿，却不想被人叫住，并硬拉着坐到一起。

婚后光子，没有加入女王大人的派系，是某个小圈子的核心人物，圈子很小，只有三四个。

不知怎么，婚后同学对她起了兴趣，多次搭话，邀请，都被她敷衍搪塞，逃避过去。御坂可不想当小圈子的小弟，给人伺候。

这次，婚后同学意外的坚持，说只是吃个饭，而且她也没有好的理由拒绝。

婚后光子没有想到会在这里遇到御坂前辈，御坂前辈是传说中的那个常盘台惟一的低能力者，之前的几次见面，都很让她放心不下，虽然是前辈，但是一直是一个人，虽然她也常见黑子一个人，但是黑子一直没心没肺，校外还有朋友同事，还常跟她对着干，完全不用担心，可是御坂前辈倔强，固执，将自己与别人隔离开来，孤立无助。

所以，她常主动搭话，有时拉着几个小伙伴，可是都不能打开前辈的心扉，完全帮不了忙，这让她很受挫。

而今天，原本她已经吃过饭，可是，在校门口看到黑子跟食蜂前辈，心里觉得难受，长得小孩子样的白井黑子都开始体验爱情了，而成熟端庄的婚后光子却还没有一点迹象。

这点难受，多吃点东西便能抹平，于是她来到了这里，碰到了一眼就看出难受无比，已经快要哭出来的御坂前辈。

前辈比她更加难过，这可不是多吃点食物便能过去的。究竟前辈为什么而难过？

不放心，所以便拉着一起吃饭。

“前辈，慢点吃，就算再难过，也不能吃得太快，对身体不好。”

却不想她的话引发了御坂美琴的哀伤，御坂前辈将头埋在双臂趴在桌上一动不动，一声不响。

是在哭泣吧？不想让人看见流泪的自己？

婚后光子一时不知该怎么安慰，只能忧心忡忡地看着，感到跟以前与御坂前辈交谈时一样的有心无力，如果黑子在这里就好了，虽然不想承认，但那家伙确实很可靠，总有办法。

难过，哭一下，应该会好受一点吧？

打破沉默的是御坂前辈的手机铃声，婚后光子松了一口气，看着别人哭，也很不好受，她完全不知道该怎么应付这种情况。

只见前辈抬头取出手机，眼睛还有些泪痕，深呼吸了几下，才接电话，声音没有什么异常。

“黑子。”

给前辈打电话的居然是黑子，什么情况？黑子跟御坂前辈认识，她们什么时候结识的，她怎么不知道，她应该是最了解黑子的啊？

难道是黑子惹前辈伤心？前辈这么难过，难道是爱上黑子，刚刚也看到那一幕？

真是罪孽深重的黑子啊，何德何能让两位前辈喜欢，如此艳福，居然还敢两条船，让御坂前辈这么伤心。

难过的还不止前辈，她也难过，由于担心前辈而被淹没的难过再次袭来，她忍不住狂吃了几口，没想到在爱情上，黑子领先她那么多。

不对，黑子不是那样的人，而且御坂前辈的表现更像是被打击的暗恋。

黑子，你居然还被暗恋？啊，婚后光子已经完全比不上黑子了。

一想到黑子的爱情如此丰富，婚后光子难过地将她点的全收进肚子里。

“嗯，我还在吃饭，吃完饭后就会宿舍，到时候再联系。”

看着御坂前辈挂掉电话，婚后光子觉得应该说些什么，可是，说什么呢，她不是爱情专家，不知道该如何处理，是要劝前辈赶紧丢掉这份感情，还是要前辈去横刀夺爱，还是说些一定没错的客套话，说前辈想怎么做，光子都支持？

“抱歉，前辈，光子猜想，前辈那么伤心，是因为前辈暗恋的人已经有对象了吧？”

“啊…嗯。”

“我也不知道该怎么给前辈出谋划策。不过，既然前辈那么喜欢，喜欢的感情那么强烈，无论对方怎么想，怎么回复，前辈都应该将这份情感传达给对方知道。”

“传达给对方？”

“是啊。抱歉，不小心多事了。”

“没有，多谢你的好意，我知道该怎么做了。”

(6)

姐姐大人的声音就是好听，直接传达到黑子的心底里去。

不过姐姐大人的声音好像有点异常，那应该只是黑子太过在意，多心的缘故吧。

黑子不舍地将手机挂掉，需要再过一会才能见到姐姐大人，可是还要再等一会才能见到可爱的姐姐大人。

怎么等呢？

抱着姐姐大人的小熊等吧。姐姐大人抱着睡觉的小熊，尽是姐姐大人的味道。太幸福了。

却不想来了电话打扰黑子的幸福，是初春，喊她过去，这个时候，难道遇到了连恒温死神都处理不了的难题？

不敢有所迟疑，黑子留下字条，便马上将自己空间传输过去。

不止是初春，佐天也在，看到佐天泪子，黑子便知道不会有什么危险事情了。

一见面，初春就质问起她来：“黑子，你怎么能这么对待御坂前辈！”

“什么情况？我不明白。”

喜欢收集都市传说的佐天说明了情况：“都市传说都传开来了，无敌黑子跟食蜂女王在约会谈恋爱。”

“这要是传到御坂前辈的耳朵里，前辈非难过死不可。”

“这都传的什么啊？你们别乱信，就是拜托了下食蜂关于前辈的事，怎么就传成这样了。都市传说不可信。这也传得太快了吧，才是下午的事情啊。”

“真的？”

“当然了。我可不是那样的人，你们要相信我。”

“怎么听着像是花花公子的狡辩？”佐天，你别捣乱啊，平时黑子可没怎么打扰你跟初春的二人世界。

“我对前辈那是天地可鉴，日月可明，绝无二心。”黑子也觉得越说越像是电影里人渣男的台词。

“好了，目前相信你，不过还有待观察。”

“初春，亏我们还是多年的朋友，战友啊。不说这个没影的都市传说了，你们也来帮我处处主意，怎么向御坂前辈表白好？”

“什么？黑子，你跟前辈还没表白过？”

“有必要这么惊讶嘛！不都一直说了，只是朋友而已，当然，现在不止想做朋友，想让前辈接受我的心意。”

“我们还以为你只是因为害羞所以才遮遮掩掩呢。”

“是啊，只是朋友，怎么能这么亲密，总是牵着手，还当街搂搂抱抱，大放闪光。”

“朋友间牵牵小手，抱来抱去不是很正常嘛。”

“哪里正常了？一般朋友不会这么做吧。”

“佐天，你好意思说我，在你还没跟初春成为情人前，便已经是掀裙狂魔了，朋友间是绝对不会做这个的，绝对。”

“好了好了，你们两个，朋友间一般都不会这么亲密。还有掀裙什么的，无论朋友还是情人，一般都不会这么做吧。都是泪子胡来，说多少遍都不改。”

“原来如此啊。”抱歉，姐姐大人，黑子居然不懂这个。下午也对不起食蜂女王，占了食蜂的便宜。

“反正你也不讨厌，是吧。”

“讨厌啦。”

“别，你们先说我的事，再二人世界好吧。”

“泪子做什么事，我都不反感。前辈明显也没讨厌你的不正常举动，也是很喜欢你的。”

“是啊，你们两个互相喜欢，这太明显了，自然而然地表达出自己的心意就行了。”

“自然而然地表达啊？好忽悠的样子，有没有具体一点的，你们两个都是过来人了。”

“要不，你也可以等前辈先主动？”

一听这个，黑子便明白了，当年是佐天开始的。“可是，我等不下去了。就像你们说的，一般朋友不会这么做，难道在前辈表白前，我都要忍着？我可受不了。”

“这样吧，明天晚上东郊区有烟火大会，在璀璨的烟火下，幽静美妙的场所里，互相深爱的两人，自然而然地吐露对对方深深的爱意，天时地利人和，完美，一定可以的。”

“还是佐天有经验有办法。便这么办。”

确实可行，不过从现在到明天晚上便需要再忍耐一下了，她忍得了不？

“或许等不到那个时候，我便把事情给办了呢。”

“那就太好了，佐天和我会在精神上支持你的。”

太好了，有初春和佐天当死党，黑子有什么事情都能解决。

“话说，在我不在的时候，你是不是也对初春那么亲密？”

生气的佐天好可怕。

“那都是小学一二年级的事情了，你吃什么醋啊。”

(7)

告别婚后光子，回到宿舍的御坂美琴没有看到黑子，只见黑子留下的纸条。

她跟黑子没有缘分吗？

她下午要找黑子的时候，没有找到，好不容易找到，却见黑子跟女王大人在亲密。

刚刚，在婚后同学的鼓舞下，鼓足了勇气，下定了决心，只要一见到黑子，她便马上吐露对黑子的爱意，可是黑子却不在。

鼓起的勇气，很快便在等待中耗光，坚定的决心也动摇了。

然后黑子回来了，没有一扑而上，而是在门口跟她互相看着，互相等待着。

“姐姐大人，我……”

“黑子，我……”

“姐姐大人，先说。”

“我……只是想欢迎黑子回来。黑子有什么事？”

“我……只是想向姐姐大人问好。”

“……没事的话，那我要洗澡了。”

“恩。”

气氛怎么这么尴尬，以前明明很欢快轻松，无拘无束的。

御坂美琴胡乱地给身体冲着水。

黑子会不会突然冒出来？如果黑子这时突然冒出来，她不会喊她出去，而是拥抱黑子，告诉黑子，她是多么的喜欢黑子。

可是黑子一直都没有冒出来。

也是，黑子有了女王大人，怎么能再对别的女孩子轻浮。

这时，御坂美琴才发觉，刚刚黑子对她的称呼变了，跟梦中一样，是“姐姐大人”。

梦中，黑子离开了她，现实中，黑子是不是也要离开她？

“姐姐”，不止是朋友，却是姐妹，不能相爱。

在外边的白井黑子，听着水声，只能不断地按耐住自己。

以前不知道，现在知道了，所以要有顺序，先表白，姐姐大人同意了，再亲密。

对，对，先表白，先表白。

可是一看到姐姐大人，便忘了想好的话，以前明明说“最喜欢前辈了”说的那么顺口，现在却是怎么都无法从口中挤出“喜欢”两字。

以前是朋友的喜欢，现在是恋人的喜欢，不一样。

哦，刚刚不小心把称呼为“姐姐大人”，如此亲密，一般朋友不会这样吧，现实中只有到达《圣母在上》祥子跟祐已那样的程度才会吧，不小心又乱了顺序。

真想马上，立刻，瞬间拥抱姐姐大人。

可是，先要告白。

可是，告白的话语却是怎么都吐露不出口。

是环境问题吗？如果漫天烟花，立身花海，寂静深幽，便可以吧？

看来还是得等到明天晚上。

当御坂美琴出浴室的时候，黑子完全不敢看一眼，怕轻轻的一眼，便不小心乱了顺序，瞬移进入浴室，让冷水冲淡内心的狂乱，虽然效果很小。

黑子，黑子，御坂美琴在内心不断地念着心中人的名字。

今天的黑子很反常，是因为跟女王大人的约会吗？

从浴室传来了黑子的声音。“姐姐大人，下午跟食蜂谈好了，她会再跟学校再商谈一下。”

“上次不是没谈妥吗？”

“这次不一样，食蜂有充分的理由和准备。怎么想，因为学生能力低就将学生退学都是错误的。放心吧，姐姐大人，交给食蜂完全没问题。”

可以听出，黑子很信任女王大人。这次，御坂也认为没问题，因为是黑子拜托的，如果是她，那么女王大人会尽全力帮忙，可是如果是作为恋人的黑子拜托的话，那么女王大人会作百分之两百，三百，乃至一千的努力。

可以留下来了，可是她却并不觉得高兴，现在留下来又有什么意义。跟没那么喜欢她的黑子同居又有什么能高兴的？

一想到这个，御坂美琴便完全失去了告白的勇气，就维持现状吧，表白后，只怕连住在一起都不行了。

睡吧，睡一觉，当作今天什么都没有发生。

当白井黑子洗完澡，黑子见到她的姐姐大人正在睡觉。

她不敢多看，再看一眼她便会忍不住扑上去。

关灯。

怎么办，她太兴奋了，一点睡意都没有，整副心思都在想着明天的事，想着明天的烟花璀璨，迷人的姐姐大人，吞吞吐吐表白的自己。

不对，不能吞吞吐吐，而是深情满满，无比自然地流露出爱意。

可是如果在完美的环境中还是怯场，怎么办？

对了，现在先写下来，明天有了底稿，肯定没问题。

蹑手蹑脚地打开书桌上的台灯，取出信纸，黑子把所有的话语都倾泄出来，她对姐姐大人的歉意，无可自拔的爱，每次见面相处的情景，黑子所有的一切心思，都想让姐姐大人知道，都想传输到御坂美琴的心底里头，打动她最最挚爱的姐姐大人。

“姐姐大人：……”

御坂美琴只是在装睡，背对着黑子闭着眼睛，虽然看不到黑子，但是心底里装满了黑子，怎么都无法将黑子踢出脑海。

已经不可能忘掉对黑子的情意，果然，她还是要对黑子表达这份情意，否则，她完全无法正常地生活。

黑子……想着想着，御坂美琴不知不觉进入梦乡，梦里黑子用爱回应她，她们过得很幸福。

姐姐大人……写着写着，白井黑子不知不觉趴在书桌上睡着，梦见姐姐大人被她感动，成为了她女朋友，她们每天都拥抱在一起。



