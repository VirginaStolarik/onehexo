---
uuid:             e782180d-5bf3-4ed1-b6c1-f4e3f8ee6dd3
layout:           post
title:            杂记：神曲中的十天
slug:             other-ten-days
subtitle:         null
date:             '2016-01-12T20:08:35.000Z'
updated:          '2016-01-12T20:08:35.000Z'
author:           'Yvan Hom'
author_slug:      yvan
header_img:       null
status:           published
language:         en_US
meta_title:       null
meta_description: null
tags:
  - 3d-world

---

参考[资料](http://cn.ieera.org/Material/Content_View.asp?ID=16714)。

* 第一月天
* 第二水星天
* 第三金星天
* 第四太阳天
* 第五火星天
* 第六木星天
* 第七土星天
* 第八恒星天
* 第九原动天
* 第十至高天

当看到百科中的第九原动天，第十至高天时，察觉到了霸气，于是搜索了半天，都没有找到其他天，在几乎要放弃时，才看到神曲的十天，前边的八天都好普通，好平凡，失望。
