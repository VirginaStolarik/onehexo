---
uuid:             52db3120-ba4b-4fd8-82eb-784e8aeef2ff
layout:           post
title:            '音乐：暗与帽子与书之旅人 OP'
slug:             music-op-yami
subtitle:         null
date:             '2015-11-28T11:55:27.000Z'
updated:          '2015-12-11T04:26:01.000Z'
author:           'Yvan Hom'
author_slug:      yvan
header_img:       null
status:           published
language:         en_US
meta_title:       null
meta_description: null
tags:
  - 2d-world
  - music

---

回顾动漫“暗与帽子与书之旅人”，最喜欢的还是 OP 音乐。

播放地址： [网易云音乐](http://music.163.com/#/song?id=598171)

尝试下外链播放器：

<iframe frameborder="no" border="0" marginwidth="0" marginheight="0" width=330 height=86 src="https://music.163.com/outchain/player?type=2&id=598171&auto=0&height=66"></iframe>

从[百度知道](http://zhidao.baidu.com/link?url=p_Xrl0QDzsLud0ZZrlhhPz4OFSt_9HOeel0lvnr2eXyi2WHKwx8wtJbzNaGOjbzkw8NP-ptLiJyyv-NqnJ_SYK)获得的信息：

```
瞳の中の迷宫
[ヤミと帽子と本の旅人 OP]

歌 ：嘉阳爱子
作词：Kenn Kato
作曲：渡部チェル
编曲：ats-Chorus
Arrangement:渡部チェル

会いたい...会えない 思い募るほど
想见你……却遇不见 思念越来越强
この祈りが届かないのは
这个祈祷不能传达
なにかをきっと 见失ってるから
必定是遗失在某处了。。

いつだって この瞳に 映されたものだけが 唯一の真实だと 信じていたけど
一直也相信着无论何时都被这双瞳反映的东西 才是这世上唯一的真实
世界は一つじゃない 「もしも」という世界 无限にあると知って
但是世界或许并非只有一个 当知道有无限『假设』的世界
いま途方に暮れている
所以现在我们在旅途中
会いたい...会えない 悔やみきれないよ
想遇见你。。却遇不上。。悔恨无法中断
この思い传えるまで
一直到这份思念能传达为止
负けない、投げない この身を捧げても
不能认输 不能放弃 即使连这身体也奉献上
见つける
也要一直寻找
たとえ暗暗が 行く手をさえぎり
纵使是被黑暗遮住去路
弱气なココロ惑わせても
而感到迷惑的脆弱心灵
あきらめないで 探し续けるから
也不能放弃 继续往前探索

そうキミを 见つけるのが 自分を见つけること
看着这样的你 就好像在看着自己
交わしたあの约束は いまも忘れない
曾经交换的那个约定 现在也无法忘记

いくつも出逢いがある 何度も救われた
几次擦肩而过 记不清几次在险中被救
すべてが勇气になる わたし一人きりじゃない
全部化作勇气 发觉原来我并不是独自一人

会いたい...会えない それが试练なら
想遇见你。。却遇不上。。倘若这是试炼的话
必ず越えていくから
定必把它超越
负けずに、投げずに わたしを待っていて
不能认输 不能放弃 她一定在前方等着
信じて
我相信
会いたい...会えない 思い募るほど
想见你。。。却遇不见 思念越来越强
この祈りが届かないのは
这个祈祷不能传达
なにかをきっと 见失ってるから
必定是遗失在某处了。。

会いたい...会えない いまどこにいるの？
想遇见你。。却遇不上。。现在的我究竟在哪里？
必ず见つけ出すから
必定找出来
迷い込んだのは 瞳の中にある迷宫
闯进了在瞳孔之中的迷宫
たとえ暗暗が 行く手をさえぎり
纵使被黑暗遮住去路
弱气なココロ惑わせても
而迷惑的脆弱心灵
あきらめないで 探し续けるから
也不能放弃 继续往前探索
```
