---
uuid:             3c97db14-9652-4c18-9694-4e615cdac4b8
layout:           post
title:            动画：京吹，停不下来的第八集
slug:             gif-huangqian-linai
subtitle:         null
date:             '2015-05-29T15:38:25.000Z'
updated:          '2016-07-04T08:01:07.000Z'
author:           'Yvan Hom'
author_slug:      yvan
header_img:       null
status:           published
language:         en_US
meta_title:       null
meta_description: null
tags:
  - hibike-enphonium
  - 2d-world

---


看了“吹响！悠风号”的第八集，完全停不下来，然后又连续看了几遍。第二天又再看了几遍。

真是病了，只想要更多更多的药。

希望接下来的几集也能这么“丧心病狂”，或者更甚。

![吹响！悠风号](http://o9sak9o6o.bkt.clouddn.com/comics/hibike-2.gif)

![吹响！悠风号](http://o9sak9o6o.bkt.clouddn.com/comics/hibike-3.gif)

![吹响！悠风号](http://o9sak9o6o.bkt.clouddn.com/comics/hibike-4.gif)

![吹响！悠风号](http://o9sak9o6o.bkt.clouddn.com/comics/hibike-5.gif)

