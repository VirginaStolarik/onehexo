---
uuid:             8015dc17-3cdb-4018-bcb7-ba94c7170ea1
layout:           post
title:            动漫：夏色奇迹
slug:             anime-natsuiro-kiseki
subtitle:         null
date:             '2015-11-23T07:37:48.000Z'
updated:          '2016-07-04T08:25:16.000Z'
author:           'Yvan Hom'
author_slug:      yvan
header_img:       null
status:           published
language:         en_US
meta_title:       null
meta_description: null
tags:
  - 2d-world

---

喜欢的动画，太赞了。

以前查[百合动画](http://baike.baidu.com/view/712474.htm)的时候，这个在名单内，但是把她错当成另一部已经看过的泡面番 [AIURA](http://baike.baidu.com/subview/9312084/10413057.htm)，而错过了不少次。汗，我都不知道自己怎么会把这两者弄混淆的。不过还好，并没有错过这部良心百合番。

看这部番，我是第一次看动画感觉到作画崩坏。以前看大家说什么动画崩，觉得很奇怪，因为我看时并不觉得，看他们的截图才有感觉。可是看夏色奇迹，真是无法直视的崩。人物表情崩坏，有一级人物的脖子特长，实在太诡异了。先是在 B 站看 BD，有点卡，换普通 TV，崩得难受，重新换会蓝光，还是崩，不过，我忍了。一口气看完。

最喜欢的是纱季，一开始并不心水，第二集的高闪让我喜欢上她，双人三足，刘翔跨栏，最后落水，太逗还闪了。第三集的天使属性让我彻底喜欢上了她，只想要她的戏分多一点。小纱季使双节棍，更是萝莉控的福音。消失的那集福利好多。真可靠，以致我都想说服自己她是外受内攻。

最不喜欢的是优香。虽然最开始喜欢的是她，第一集的表现，给我一股这家伙很灵动，第二集的奔跑表现更是萌得很。可是借着纱季的身体约会，还差点 kiss，比赛只想着神石许愿，自己不加练习，黑点不少。不过还好，有凛拉着，有纱季教训着，有夏海看着，所以一切都还很美好。

凛，三无，貌似有点黑，都很戳我的萌点。她对优香的喜欢没有人能比得上，对团队每个人的理解，信任都是最高的。

夏海，感觉是最没存在感的，但是她跟纱季的 CP 太美好了。

OVA 中，夏海呼唤着纱季奔跑而来，纱季慢镜头特效回头，制作组，我懂你，她们就赶快结婚吧。

[豆瓣连接](http://movie.douban.com/subject/6984049/)，[B 站播放连接](http://www.bilibili.com/video/av466906/)

貌似冷门到只有一本同人本，搜索了半天，没有找到，遗憾。

![滚床单](http://o9sak9o6o.bkt.clouddn.com/comics/xiaseqiji-1.gif)
![绝对的吸引力啊](http://o9sak9o6o.bkt.clouddn.com/comics/xiaseqiji-2.gif)
![相视而笑，误会冰释](http://o9sak9o6o.bkt.clouddn.com/comics/xiaseqiji-3.gif)
![夏海：老婆好美](http://o9sak9o6o.bkt.clouddn.com/comics/xiaseqiji-4.gif)
![夏海：老婆好可爱](http://o9sak9o6o.bkt.clouddn.com/comics/xiaseqiji-5.gif)
![小纱季好萌](http://o9sak9o6o.bkt.clouddn.com/comics/xiaseqiji-6.gif)
![纱季](http://o9sak9o6o.bkt.clouddn.com/comics/xiaseqiji-7.gif)

