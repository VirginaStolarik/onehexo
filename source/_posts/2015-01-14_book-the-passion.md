---
uuid:             e36bcacd-6ad2-4c80-bb86-744fe5f4049c
layout:           post
title:            读书：激情
slug:             book-the-passion
subtitle:         null
date:             '2015-01-14T02:23:26.000Z'
updated:          '2015-07-31T06:53:45.000Z'
author:           'Yvan Hom'
author_slug:      yvan
header_img:       null
status:           published
language:         en_US
meta_title:       null
meta_description: null
tags:
  - 3d-world
  - reading

---


珍妮特•温特森的《激情》，比不久前看的《苹果笔记本》容易看得多了，基本上便是一气呵成，从头看到尾，没有停顿，为书中人物的命运所感，渴望看到她们能有一个好结局。附上[豆瓣链接](http://book.douban.com/subject/5940877/)。

结局也是悲伤的，维拉内拉疯狂爱着的是别人的夫人，而这位夫人还爱着她的丈夫，无法为她抛弃丈夫与家庭。亨利激情地爱上维拉内拉，为此不惜杀死维拉内拉的丈夫，可是维拉内拉的心已经给了别人，无法再给他，纵使他们已经有了孩子，他最终选择待在疯人院，与幻想为伴。

故事发生在拿破仑时代，拿破仑对鸡肉的激情，对胜利的追求，鼓动法国人们参加无休止的战争，都描写地非常生动，虽然对历史人物来说还是比较戏谑。

亨利对拿破仑的爱，让他盲目地追随拿破仑八年，结果发现拿破仑并不值得，于是便转换为恨，与维拉内拉走上了逃兵之路。

从这一段开始，我深深感受到了维拉内拉的魅力，神秘，勇敢，知性，智慧，有如女神，可是另一个女神呢？

一直在期盼着维拉内拉跟她能再继缘分，终于等到了，却只是匆匆一面，然后便不再相见。

急切的希望能跟《苹果笔记本》中的阿里，能够说出放下过往的一切，我跟你再开启新的生活，而不是永远的道别。虽然这对不起亨利，对不起她的丈夫和家庭，但是激情瞬间，又有什么对错。



