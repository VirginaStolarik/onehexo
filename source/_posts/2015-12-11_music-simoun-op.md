---
uuid:             b1613285-4080-44ea-90d9-a61b6984f97d
layout:           post
title:            '音乐：Simoun OP'
slug:             music-simoun-op
subtitle:         null
date:             '2015-12-11T05:46:48.000Z'
updated:          '2015-12-11T09:33:46.000Z'
author:           'Yvan Hom'
author_slug:      yvan
header_img:       null
status:           published
language:         en_US
meta_title:       null
meta_description: null
tags:
  - 2d-world
  - music

---

[Simoun](http://movie.douban.com/subject/2049766/) 的 OP，美しければそれでいい。

[酷我在线播放](http://bd.kuwo.cn/yinyue/2331831)。

中文翻译，美则无求

好吧，翻译的完全不好。

这首音乐在我刚看完 Simoun 后，单曲循环了好多遍，好多天，甚至还自己学唱了前边几句，喜欢上了石川智晶。

音乐中，我听出了无奈，听出了悲哀，结合动画剧情，相当的共鸣。
