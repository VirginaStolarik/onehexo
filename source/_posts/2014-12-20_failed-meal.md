---
uuid:             4df18688-0034-4191-9e5c-d0b8d88df1ac
layout:           post
title:            失败的番茄饭
slug:             failed-meal
subtitle:         null
date:             '2014-12-20T08:57:54.000Z'
updated:          '2016-07-04T07:29:33.000Z'
author:           'Yvan Hom'
author_slug:      yvan
header_img:       null
status:           published
language:         en_US
meta_title:       null
meta_description: null
tags:
  - 3d-world
  - foods

---


本来上周就想做的，可是太忙，没时间，于是今天买了食材，努力整了一顿。

结果以失败告终。一两口还能吃，一碗就感觉再也吃不下去，勉强吃两碗，反胃，想吐。

![](http://o9sak9o6o.bkt.clouddn.com/cooks/fanqiefan.jpg)

罪过啊，自己折磨自己。

向老姐请教，总结经验教训：

- 水放多
- 饭煮过头

再接再厉！

尽头还去买了干子，加热，绊点酱油，还是能吃的。

![](http://o9sak9o6o.bkt.clouddn.com/cooks/ganzi.jpg)



