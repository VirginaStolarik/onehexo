---
uuid:             04ff7c16-b00a-4bef-8bb2-00238a3c5468
layout:           post
title:            电影：二重身
slug:             movie-bilocation
subtitle:         null
date:             '2015-03-02T11:39:54.000Z'
updated:          '2015-07-31T06:58:04.000Z'
author:           'Yvan Hom'
author_slug:      yvan
header_img:       null
status:           published
language:         en_US
meta_title:       null
meta_description: null
tags:
  - 3d-world
  - movies

---


《二重身》，韩国悬疑片，附上[豆瓣链接](http://movie.douban.com/subject/22299426/)。

看完挺难受的。

因为自己本身并不和群，一直羡慕那些忍受寂寞多年，憋出大招的大神。

可是这样的大神是越来越少了，在现代社会，没有别人帮助，是什么事情都做不到的。

看着女主本体把自己孤立起来，拒绝跟不相关人的深入接触，全心全意放在画作上，结果输给了完全不把画画事业当回事，一心只想当个好妻子的分身上，真心难受，也难怪女主本体要跳楼自杀，结束自己失败的人生。

本片有两个结局，一个是本体死后，分身也消失，另一个是分身没有消失，由于怀上新生命而让自己也获得了新生。看着后一个结局真心难受，适者生存，难道与社会相适应者便能重生，与之不适应便只能消失吗？

分身丈夫，我还是很欣赏的，爱女主分身，爱分身的画作，所以当分身一心只想当好妻子，要丢弃画画事业时，真心对她没有好感，亏你还得了奖。

莫名想起《心花路放》中的耿浩，我想女主出轨的原因是男主放弃了自己的音乐梦想。

分身，难道你一从本体分出来，便丢掉了自己的艺术追求了吗？

正是因为这方面的追求，影片开始时那个全身邋遢，面容不整的画师才显得美丽动人。

回望自己，追求不坚定，总是四处分心，奔波迎合，已经完全看不到自己有何出路！

难过。



