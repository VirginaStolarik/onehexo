---
uuid:             a7108bf5-9f88-41c1-908a-691e47181ccf
layout:           post
title:            电影：法老与众神
slug:             movie-exodus-gods-and-kings
subtitle:         null
date:             '2015-03-16T11:01:14.000Z'
updated:          '2015-07-31T06:59:16.000Z'
author:           'Yvan Hom'
author_slug:      yvan
header_img:       null
status:           published
language:         en_US
meta_title:       null
meta_description: null
tags:
  - 3d-world
  - movies

---


这影片真心不合口味，附上[豆瓣链接](http://movie.douban.com/subject/10793610/)。

以前看圣经时，便感觉到上帝的所作所为完全是出于玩弄人民，毫无道理，要拯救犹太人奴役的命运，方法多的是，尤其是对于万能的上帝来说。十个灾难完全便是激怒人的，法老王的应对办法便是看谁杀得多，杀得快，即使是最后关头，也不放弃，千里追杀，哪里看出上帝的睿智？

感觉电影也是这么认为的，于是上帝变成了小孩子，总不能要求小孩有多高的智商吧。

摩西戏分虽然多，但是我怎么看都觉得他是打酱油的，有他没他，都无关系，上帝需要他做什么别人做不到的？

在电脑上看的，所以也不知道有啥神奇的3D特效。

不说了，不喜欢的影片，就这样遗忘了吧。



