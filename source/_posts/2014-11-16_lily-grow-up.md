---
uuid:             5ff0b3b1-8106-4810-8d7a-e65840bd7c67
layout:           post
title:            长大的百合
slug:             lily-grow-up
subtitle:         null
date:             '2014-11-16T11:00:00.000Z'
updated:          '2016-07-04T07:17:24.000Z'
author:           'Yvan Hom'
author_slug:      yvan
header_img:       null
status:           published
language:         en_US
meta_title:       null
meta_description: null
tags:
  - 3d-world
  - plants

---


仅仅一周多过去，百合种球长高了很多。

一周前还是这样，用尺子量了一下，差不多20+厘米：

![](http://o9sak9o6o.bkt.clouddn.com/plants/lily-0.jpg)

现在已经这么高了，接近40厘米了：

![](http://o9sak9o6o.bkt.clouddn.com/plants/lily-1.jpg)

真心神奇。



