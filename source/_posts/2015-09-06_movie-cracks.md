---
uuid:             a98c6da1-1297-4ac6-86dd-eb172e918074
layout:           post
title:            电影：裂缝
slug:             movie-cracks
subtitle:         null
date:             '2015-09-06T23:43:22.000Z'
updated:          '2015-09-06T23:43:22.000Z'
author:           'Yvan Hom'
author_slug:      yvan
header_img:       null
status:           published
language:         en_US
meta_title:       null
meta_description: null
tags:
  - 3d-world
  - movies

---

多年前看过一遍，忘了不少，近日重看一遍，依然非常震撼，感触很深。[豆瓣链接](http://movie.douban.com/subject/3007817/)

一句话形容便是封闭真可怕，欺骗无处不在。越小的封闭越可怕，因为已经难以自欺欺人了，只能通过一些手段来进行封闭。

走出封闭，需要勇气。

而从外边的世界进入一个封闭的环境，则非常危险。

新事物与老事物相互摩擦，新血液与权威之间相互挑战。

有时，不是退一步便能海阔天空，而是得寸进尺，忍一时不能风平浪静，而是浪滚浪，直到死在沙滩上。

于是悲剧发上了。

有时，只有流血才能带来改变。

老师被赶出了学校，可是依然改不了遵守学校规定的习惯，黛带着愧疚走出了学校，要去看看那片新天地，这才是新的希望。
