---
uuid:             382ae0ec-9e06-468f-b952-751ed149aa01
layout:           post
title:            猪骨菠菜粥
slug:             one-porridge
subtitle:         null
date:             '2015-01-23T00:47:31.000Z'
updated:          '2016-07-04T07:39:23.000Z'
author:           'Yvan Hom'
author_slug:      yvan
header_img:       null
status:           published
language:         en_US
meta_title:       null
meta_description: null
tags:
  - 3d-world
  - foods

---


N天前做的，拍照后便忘了上传发表。

先炖猪骨，泡米，炖得差不多，煮粥，煮得差不多，加菠菜，沸腾后加调料，完成。

味道不错。

发现我煮粥比煮饭好吃。

![](http://o9sak9o6o.bkt.clouddn.com/cooks/zhugubocaizhou.jpg)

![](http://o9sak9o6o.bkt.clouddn.com/cooks/zhugubocaizhou-2.jpg)



