---
uuid:             55cb9739-cd57-42ba-8845-e1248f50a876
layout:           post
title:            电影：两个女人
slug:             movie-two-women
subtitle:         null
date:             '2015-01-21T05:40:27.000Z'
updated:          '2015-07-31T06:54:11.000Z'
author:           'Yvan Hom'
author_slug:      yvan
header_img:       null
status:           published
language:         en_US
meta_title:       null
meta_description: null
tags:
  - 3d-world
  - movies

---


“你知道我什么时候最爱你吗？”

“……”

“你只爱我一个人的时候！”

爱情是两个人的，当加入第三个人后，便会出现裂缝，三角形是最稳定的，这在爱情上并不成立，三角恋苦的是三个人。

男人：我爱你，矢志不渝，到是她是我不论一切都要保护的。

爱着情人，给妻子保护，男人想要保持着这一状态，可是该状态已经岌岌可危。

妻子在偶然中发现了男人的出轨，并很快找到了情人，情人没她漂亮，没她聪明，有的是比她青春活力，是什么让男人出轨？

妻子伪装后跟情人接触，并很快成了知心朋友，互述衷肠，情人将跟男人的一切都吐露，爱着已婚男人不能自拔，却无法改变现状，妻子也讲述了自己为丈夫背叛。

妻子：你值得被爱。

情人：你真好，你老公真没眼光。

都是好女人，都值得被爱，都该享受爱情的甜蜜，而不是苦涩。

最终，

男人做出了抉择，选择妻子，不得不抛弃情人，即使情人有了他的孩子。

妻子也做出了决定，离开男人，成全他们，对着男人说：“她喝了酒，还有身孕，快去找她。”

情人也做出了选择，即使男人找到了她，也坚定决心，不去留恋，转身离去。

最终，男人因为喝了加了麻醉药的水而被冻死在野外，留下两个女人。

身为大夫的妻子在情人将要早产需要切腹手术时起了犹豫，但很快便全心投入，可以说，两个女人之间已经不再被那个男人所累了。

不是男人的妻子跟情人，而是真挚的朋友。

希望她们能幸福。

附上[豆瓣链接](http://movie.douban.com/subject/5285675/)。



