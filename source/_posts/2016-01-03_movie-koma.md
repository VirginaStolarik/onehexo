---
uuid:             f0954169-c848-439d-b752-132f1d060071
layout:           post
title:            电影：救命
slug:             movie-koma
subtitle:         null
date:             '2016-01-03T05:27:58.000Z'
updated:          '2016-01-03T05:27:58.000Z'
author:           'Yvan Hom'
author_slug:      yvan
header_img:       null
status:           published
language:         en_US
meta_title:       null
meta_description: null
tags:
  - 3d-world
  - movies

---

不是恐怖片，是悬疑片。

人的出生，注定了起跑线的不同，人生路线的不同。像孙玲，出生不好，毕业后做着普通的工作，然后遇上母亲变成植物人，把所有能借到的钱都借了，成为男人玩弄并侮辱为“贱货”的泄欲工具。另一边，子清，出生好，无忧无虑，美中不足的是肾有问题，但并不妨碍男朋友把她当女神，就算连碰都不能碰。

两个人，因为偷肾买卖，因为一个男人而碰撞到一起。子清善良地想要帮助孙玲，孙玲则嫉妒，羡慕，而又鄙视。错综的情感交错，是爱是恨。是宽恕救赎，还是怨恨报复，都不可避免走上极端。

结局注定悲剧。那个男人是人渣，孙玲偷肾的罪行必遭报应，子清则注定要背负这一切后果，活下去，身体里有孙玲的肾，那个女人，将永生跟随着她，嘲弄命运。


相关链接：

* [豆瓣视频](http://movie.douban.com/subject/1308797/)
* [B 站观看](http://www.bilibili.com/video/av3482089/)
