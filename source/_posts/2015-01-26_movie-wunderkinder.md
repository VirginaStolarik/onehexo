---
uuid:             ae0dbe37-b0f6-4935-825a-9bc2abe989af
layout:           post
title:            电影：神童
slug:             movie-wunderkinder
subtitle:         null
date:             '2015-01-26T10:25:23.000Z'
updated:          '2015-07-31T06:55:06.000Z'
author:           'Yvan Hom'
author_slug:      yvan
header_img:       null
status:           published
language:         en_US
meta_title:       null
meta_description: null
tags:
  - 3d-world
  - movies

---


神童，又称爱乐时光，大时代小神童。附上[豆瓣链接](http://movie.douban.com/subject/6801386/)。

二战反思片，纪念二战中死去的150万犹太儿童。

战争反思片，总是能触动人的心灵，电影呈现无数美好的事物，然后再瞬间被摧毁，人类的残忍一览无遗，生命的脆弱毫不保留地呈现在你的面前，使得你在内心中不得不担忧战争的再次降临。

神童，两个音乐的神童，莱丽莎弹钢琴，阿布拉夏拉小提琴，女主听完他们的演奏后一直记在心底，并称为了好朋友，真挚的友情在三人间自然而然地诞生。可是孩童的友情，在残酷的战争面前，却是多么的无力，先是老人，然后是父母，最后是他们自己面对着威胁，在战争的大时代面前，音乐的神童既无法拯救他人，也无法拯救朋友，更无法拯救自己。

小小孩童便必须面对大人的残酷，莱丽莎在最后的失控，也是因为她看到的太多，心灵无法承受，在医院里，目睹德军的轰炸，美丽的钢琴瞬间变成废铁，周围所见，一片废墟；之后奶奶离开，优雅的伊丽娜老师死去，逃亡失败后，父母都被带走。

跟纳粹军官对峙时，莱丽莎故作镇定，可是实际军官给苹果削皮，切苹果的场景已经牢牢地印在脑海，她跟他，他们的家人，就像落在军官手中的苹果一样，任他削，任他切，毫无反抗余地，而稍微推想，便知道，她再也见不到她的亲人了。

于是悲伤彻底压垮了她，导致她舞台上无法再弹奏下去。

阿布拉夏跟汉娜上台抱着她，三人一起哭泣，这是三人最后的相聚了，那首友谊之歌到最后始终没有完成，也没有演奏。

当老年的阿布拉夏跟汉娜重逢，纪念死去的莱丽莎跟其它遇难者时，阿布拉夏已经不再拉小提琴，汉娜继承了两位神童的音乐之路，汉娜的孙女则展示着新的希望。

希望悲剧不再重演。

可是现在世界并不安稳。



