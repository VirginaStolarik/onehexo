---
uuid:             526ba9c0-a93b-455b-81e0-9650d0203a26
layout:           post
title:            电影：卡罗儿
slug:             movie-carol
subtitle:         null
date:             '2016-01-03T05:04:41.000Z'
updated:          '2016-01-03T05:04:41.000Z'
author:           'Yvan Hom'
author_slug:      yvan
header_img:       null
status:           published
language:         en_US
meta_title:       null
meta_description: null
tags:
  - 3d-world
  - movies

---

看完，结合弹幕，感觉自己还不够文艺，水平不到，欣赏不了。

只是，一股感觉，氛围，很暧昧，忍不住被吸引，又很压抑，忍不住不好的预感。

或许，这便是女人与女人之间的爱情吧。

相关链接

* [豆瓣链接](http://movie.douban.com/subject/10757577/)
* [B 站观看](http://www.bilibili.com/video/av3422883/)
