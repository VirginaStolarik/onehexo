---
uuid:             7771f976-bbff-47c5-a95b-75a184661a28
layout:           post
title:            百合不容易，两个花苞
slug:             lily-strong
subtitle:         null
date:             '2014-12-30T09:16:54.000Z'
updated:          '2016-07-04T08:30:49.000Z'
author:           'Yvan Hom'
author_slug:      yvan
header_img:       null
status:           published
language:         en_US
meta_title:       null
meta_description: null
tags:
  - 3d-world
  - plants

---


天气越来越寒冷，不过从10月种到现在的百合种球，倒是没有停止成长，让我欣慰。

貌似叫“特里昂菲特”，真心容易种。

现在都长出了两个花苞。

![](http://o9sak9o6o.bkt.clouddn.com/plants/lily-9.jpg)

最大的希望是能够在寒假前开花。

因为寒假就得弃置两个星期不管了。

真心没良心。



