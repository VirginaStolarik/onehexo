---
uuid:             da275f17-3725-4eef-8a0a-d1d21893bd84
layout:           post
title:            动画：复习侦探歌剧第一二季
slug:             comic-milky-holmes-2
subtitle:         null
date:             '2016-04-25T07:45:57.000Z'
updated:          '2016-04-25T07:45:57.000Z'
author:           'Yvan Hom'
author_slug:      yvan
header_img:       null
status:           published
language:         en_US
meta_title:       null
meta_description: null
tags:
  - 2d-world

---

本周末花了不少时间，复习了下侦探歌剧的第一二季，途中无数次忍不住哈哈大笑，能这么掉智商真是太开心了。

忍不住回忆了下第三季，和第四季，不得不感叹，有掉智商的前两季在，明明可以不用靠智商，为什么第三季还要找回脑子，至于第四季的回归，不得不说，太明智了，可是还我怪盗帝国，还我会长，还我亚森……

会长好温柔，好美丽，好魅力，怪盗帝国招人不？
