---
uuid:             45eabb29-9602-4593-b82b-07dce25d7606
layout:           post
title:            冬瓜猪骨汤
slug:             winter-melon-soup-with-pork-rib
subtitle:         null
date:             '2014-11-23T12:00:00.000Z'
updated:          '2016-07-04T07:20:50.000Z'
author:           'Yvan Hom'
author_slug:      yvan
header_img:       null
status:           published
language:         en_US
meta_title:       null
meta_description: null
tags:
  - 3d-world
  - foods

---


自己做了一顿饭。

主要是冬瓜猪骨汤。

不过明显失败了，错误地加了不少黄豆，味道也没体现出来，很失败。

这顿饭：米饭 + 冬瓜猪骨汤 + 橄榄菜

![](http://o9sak9o6o.bkt.clouddn.com/cooks/dongguapaigutang.jpg)



